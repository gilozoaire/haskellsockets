module MessageAssembly where

import           Data.ByteString
import           Data.ByteString.Char8         as C
import           Text.Parsec                   as P
import           Text.ParserCombinators.Parsec as Parsec
import           Types

parseHeader :: Parser Header
parseHeader = do
  lgth <- between crlf crlf $ string "contentLength:" >> many digit
  return $ Header (read lgth :: Int)

parseComplete :: Parser (BufferResult, ByteString)
parseComplete = do
  header <- parseHeader
  body <- Parsec.count (contentLength header) anyChar
  rest <- getInput
  return (Complete header (C.pack body), C.pack rest)

attemptResultExtract :: ByteString -> (BufferResult, ByteString)
attemptResultExtract str = do
  let res = P.runParser parseComplete  () "Protocol" (C.unpack str)
  case res of
    Right c@(Complete h msg, rest) -> c
    Left err ->
      case P.runParser parseHeader () "Protocol" (C.unpack str) of
        Right h  -> (Partial h, str)
        Left err -> (Garbage str, str)
