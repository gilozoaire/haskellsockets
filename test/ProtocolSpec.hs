module ProtocolSpec(spec) where

import           Data.ByteString.Char8
import           Data.Either
import           Protocol
import           Test.Hspec

spec :: Spec
spec = do
  describe "parseMessage spec" $ do
    it "parsed correct greeting string" $ do
      1 `shouldBe` 1 
