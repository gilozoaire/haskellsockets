module Main where

import System.Environment
import Client
import Server

main :: IO ()
main = do
  args <- getArgs
  if length args /= 1
    then putStrLn "This program only accepts one argument: client or server."
    else
      case head args of
        "client" -> launchClient
        "server" -> launchServer
        _ -> putStrLn "unrecognized argument, cannot start"
