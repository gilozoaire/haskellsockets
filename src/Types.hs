{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DuplicateRecordFields #-}

module Types where
import           Data.Aeson
import           Data.ByteString
import           GHC.Generics

data Header = Header { contentLength :: Int } deriving (Eq, Show)

data BufferResult =
  Garbage ByteString
  | Partial Header
  | Complete Header ByteString
  deriving (Eq, Show)

data HeartBeat = Ping | Pong deriving (Eq, Show, Generic)
data ServerMessage =
  Connected { session :: String }
  | Failed { reason :: String }
  deriving (Eq, Show, Generic)

data ClientMessage =
  Connect {
    session :: String
  }
  | Sub {
    source :: String
  }
  deriving (Eq, Show, Generic)

instance ToJSON ClientMessage
instance FromJSON ClientMessage

instance ToJSON ServerMessage
instance FromJSON ServerMessage

instance ToJSON HeartBeat
instance FromJSON HeartBeat
