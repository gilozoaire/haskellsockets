module Server where

import           Control.Concurrent
import           Control.Monad             (forever, unless)
import           Data.ByteString.Char8     as C
import           MessageAssembly
import           Network.Socket
import           Network.Socket.ByteString as B
import           Protocol
import           SockSettings
import           Types

partLength = 40
maxLength = 300

printServInfo :: AddrInfo -> IO()
printServInfo info = do
  print $ "Flags: " ++ show (addrFlags info)
  print $ "Fam: " ++ show (addrFamily info)
  print $ "Socket type: " ++ show (addrSocketType info)
  print $ "ProtocolNumber: " ++ show (addrProtocol info)
  print $ "addr: " ++ show (addrAddress info)

launchServer :: IO ()
launchServer = do
  info@(AddrInfo flags fam sockType prot addr _) <- cAddrInfo
  printServInfo info
  sock <- socket fam sockType prot
  bind sock addr
  listen sock 1
  forever $ handleConnections sock
  close sock

handleConnections :: Socket -> IO ()
handleConnections sock = do
  print "Listenning for connection: "
  (connSock, _) <- accept sock
  print $ "Received connection connection: " ++ show connSock
  forkIO $ handleConnections sock
  treatMessages (C.pack "") connSock
  print $ "Preparing to close connection: " ++ show connSock
  close connSock

treatMessages :: ByteString -> Socket -> IO ()
treatMessages buffer conn = do
  str <- B.recv conn partLength
  unless (C.null str) $ do
    print $ "received: " ++ unpack str
    let nBuff = buffer `append` str
    print $ "Currently in buffer: " ++ unpack nBuff
    if C.length nBuff < maxLength
      then case attemptResultExtract nBuff of
            (c@(Complete _ _), rest) -> treatComplete c conn >> treatMessages rest conn
            (res, rest)    -> print (show res) >> treatMessages rest conn
      else sendAll conn (pack "Too many invalid bytes received\n") >> close conn

treatComplete :: BufferResult -> Socket -> IO ()
treatComplete (Complete _ str ) sock =
  case analyzeMessage str of
    Right mess -> print $ show mess
    Left err   -> print $ show err
treatComplete _ _ = print "Cannot process any result other than Complete."
