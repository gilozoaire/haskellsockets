module SockSettings (cAddrInfo) where
import           Network.Socket

getConfiguredHints :: AddrInfo
getConfiguredHints =  defaultHints {
  addrFlags = [AI_NUMERICSERV],
  addrSocketType = Stream,
  addrFamily = AF_INET
}

cAddrInfo :: IO AddrInfo
cAddrInfo = do
  info <- getAddrInfo (Just getConfiguredHints) (Just "127.0.0.1")  (Just "3000")
  return $ head info
