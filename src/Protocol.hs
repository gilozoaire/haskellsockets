module Protocol where

import           Control.Concurrent.STM
import           Data.Aeson
import           Data.ByteString
import           Data.ByteString.Char8         as C
import           Data.Either
import           Text.Parsec                   as P
import           Text.ParserCombinators.Parsec as Parsec
import           Types

data Connection = Connection {
  session :: String
}

type ConnectionPool = TVar [Connection]

analyzeMessage :: ByteString -> Either String ClientMessage
analyzeMessage str = case decodeStrict str of
  Just c  -> Right c
  Nothing -> Left ("Could not extract client message from: " ++ C.unpack str)

treatClientMessage :: ClientMessage -> IO ServerMessage
treatClientMessage (Connect session) = undefined -- add to connection pool
treatClientMessage (Sub source)      = undefined

insertConn :: Connection -> ConnectionPool -> STM ()
insertConn c pool = do
  conns <- readTVar pool
  writeTVar pool (c:conns)

findConn :: String -> ConnectionPool -> STM ()
findConn sess pool = undefined -- TODO: find connection corresponding to session
